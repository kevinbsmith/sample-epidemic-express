# sample-epidemic-express

This repo contains sample code that I wrote back in 2015. 
I do not recommend that anyone actually use this code,
since it hasn't been updated in years. 

At the time, I had never written a single-page web application,
nor had I written anything in Javascript, so I wanted to try. 

I decided to implement a simple dice game that I had designed 
some years earlier, named Epidemic Express. 
Epidemic Express is a highly simplified game patterned after 
the wildly successful "Pandemic" boardgame. 

Not knowing which library or framework to choose, 
I ended up using dojo, which at the time seemed like 
a reasonable balance of power and simplicity. 
This was before React, Angular, or Vue had been released. 

The interesting code is in `EpidemicExpress.js`. 